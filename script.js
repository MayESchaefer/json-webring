const webringURL = "https://webring.example.net/";
const localBaseURL = window.location.origin ? window.location.origin + '/' : window.location.protocol + '/' + window.location.host + '/';
fetch(webringURL + "ring.json").then(response => response.json().then(items => {
	let content = "";
	items.forEach(item => {
		if (item.url === localBaseURL) {
			return;
		}
		if (Object.hasOwn(item, "image")) {
			content += `<a class="webring-item-image" href="${item.url}"><img src="${webringURL + item.image}" alt="${item.text}"/></a>`;
		}
		else {
			content += `<a class="webring-item-text" href="${item.url}">${item.text}</a>`;
		}
		document.getElementById("webring-items").innerHTML = content;
	});
}));
