<?php
$aboutURL = "https://webring.example.net/";
$baseURL = "https://" . $_SERVER['SERVER_NAME'];
$items = json_decode(file_get_contents($baseURL . "/ring.json"), true);

echo "<!DOCTYPE html><html><head>";

if (isset($_GET["css"]) && filter_var(urldecode($_GET["css"]), FILTER_VALIDATE_URL)) {
	echo '<link rel="stylesheet" href="' . urldecode($_GET["css"]) . '">';
}
else {
	echo '<link rel="stylesheet" href="' . $baseURL . '/iframe_ring_style.css">';
}

echo '</head><body><table class="webring-circle">';

$myIndex = 0;
for ($i = 0; $i < sizeof($items); $i++) {
	if ($items[$i]["url"] === $baseURL) {
		$myIndex = $i;
		break;
	}
}
$prev = $myIndex - 1;
$next = $myIndex + 1;
if ($myIndex === 0) {
	$prev = sizeof($items) - 1;
}
else if ($myIndex === sizeof($items)- 1) {
	$next = 0;
}
$rand = $myIndex;
while ($rand === $myIndex) {
	$rand = rand(0, sizeof($items) - 1);
}
echo '<td class="webring-left"><a id="webring-prev" href="'. $items[$prev]["url"] .'" target="_top">previous</a></td>';
echo '<td class="webring-center"><div id="webring-description">This site is part of the Thoughts of Mine webring</div>';
echo '<a id="webring-rand" href="'. $items[$rand]["url"] .'" target="_top">random</a>';
echo '&nbsp;';
echo '<a id="webring-about" href="'. $aboutURL .'" target="_top">about</a>';
echo '</td><td class="webring-right"><a id="webring-next" href="'. $items[$next]["url"] .'" target="_top">next</a></td>';
echo '</table></body></html>';
?>

