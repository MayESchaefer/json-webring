# JSON Webring

This is a simple project I threw together to make a JSON-based webring.

To setup your own, just serve a copy of this repo on a server, and use the provided example scripts on your websites to add the webring to your site!

Remember to change the example URLs in the files when you make your copy!

If you can't set the CORS headers on your server, you'll need to use the iframe example, otherwise you can use any of them.



The iframe examples accept two GET parameters:

- The first is demoed in the example: `exclude` is the root URL of the current site, to be excluded from the list in the linkroll example, or to provide an index for the webring example
- The second is not demoed: `css` is the URL for a CSS stylesheet, which replaces the default stylesheet if provided.